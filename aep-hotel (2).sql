-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2022 at 08:17 PM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aef_hotel`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_fasilitas_kamars`
--

CREATE TABLE `detail_fasilitas_kamars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fasilitas_id` int(11) DEFAULT NULL,
  `kamar_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_fasilitas_kamars`
--

INSERT INTO `detail_fasilitas_kamars` (`id`, `fasilitas_id`, `kamar_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2022-03-11 20:20:58', '2022-03-11 20:20:58'),
(2, 1, 2, '2022-03-11 20:30:34', '2022-03-11 20:30:34'),
(3, 1, 3, '2022-03-11 20:40:45', '2022-03-11 20:40:45'),
(4, 2, 3, '2022-03-11 20:40:56', '2022-03-11 20:40:56'),
(5, 1, 4, '2022-03-11 20:45:18', '2022-03-11 20:45:18'),
(6, 2, 4, '2022-03-11 20:45:29', '2022-03-11 20:45:29'),
(7, 1, 5, '2022-03-11 20:52:52', '2022-03-11 20:52:52'),
(8, 2, 5, '2022-03-11 20:53:04', '2022-03-11 20:53:04'),
(9, 1, 6, '2022-03-11 20:56:21', '2022-03-11 20:56:21'),
(10, 2, 6, '2022-03-11 20:56:30', '2022-03-11 20:56:30'),
(11, 4, 5, '2022-03-11 21:05:19', '2022-03-11 21:05:19'),
(12, 5, 5, '2022-03-11 21:05:28', '2022-03-11 21:05:28'),
(13, 4, 4, '2022-03-11 21:05:51', '2022-03-11 21:05:51'),
(14, 5, 4, '2022-03-11 21:06:02', '2022-03-11 21:06:02'),
(15, 4, 3, '2022-03-11 21:06:28', '2022-03-11 21:06:28'),
(16, 1, 7, '2022-03-11 21:09:16', '2022-03-11 21:09:16'),
(17, 2, 7, '2022-03-11 21:09:26', '2022-03-11 21:09:26'),
(18, 4, 7, '2022-03-11 21:09:36', '2022-03-11 21:09:36'),
(19, 5, 7, '2022-03-11 21:09:44', '2022-03-11 21:09:44'),
(20, 4, 1, '2022-03-11 21:10:24', '2022-03-11 21:10:24'),
(21, 4, 2, '2022-03-11 21:11:49', '2022-03-11 21:11:49'),
(22, 2, 2, '2022-03-11 21:11:59', '2022-03-11 21:11:59'),
(23, 2, 8, '2022-03-11 21:16:06', '2022-03-11 21:16:06'),
(24, 1, 9, '2022-03-11 21:17:28', '2022-03-11 21:17:28'),
(25, 2, 9, '2022-03-11 21:17:37', '2022-03-11 21:17:37'),
(26, 4, 9, '2022-03-11 21:17:47', '2022-03-11 21:17:47'),
(27, 7, 2, '2022-03-11 21:20:30', '2022-03-11 21:20:30'),
(28, 7, 3, '2022-03-11 21:20:59', '2022-03-11 21:20:59'),
(29, 7, 4, '2022-03-11 21:21:26', '2022-03-11 21:21:26'),
(30, 7, 5, '2022-03-11 21:21:52', '2022-03-11 21:21:52'),
(31, 7, 6, '2022-03-11 21:22:20', '2022-03-11 21:22:20'),
(32, 7, 9, '2022-03-11 21:22:49', '2022-03-11 21:22:49'),
(33, 7, 1, '2022-03-11 21:23:16', '2022-03-11 21:23:16'),
(34, 7, 7, '2022-03-11 21:23:52', '2022-03-11 21:23:52'),
(35, 7, 8, '2022-03-11 21:24:15', '2022-03-11 21:24:15'),
(36, 1, 10, '2022-03-11 21:25:45', '2022-03-11 21:25:45'),
(37, 2, 10, '2022-03-11 21:25:54', '2022-03-11 21:25:54'),
(38, 4, 10, '2022-03-11 21:26:03', '2022-03-11 21:26:03'),
(39, 7, 10, '2022-03-11 21:26:17', '2022-03-11 21:26:17'),
(40, 1, 11, '2022-03-11 21:27:50', '2022-03-11 21:27:50'),
(41, 2, 11, '2022-03-11 21:28:01', '2022-03-11 21:28:01'),
(42, 4, 11, '2022-03-11 21:28:10', '2022-03-11 21:28:10'),
(43, 7, 11, '2022-03-11 21:28:19', '2022-03-11 21:28:19'),
(44, 2, 12, '2022-03-11 21:29:38', '2022-03-11 21:29:38'),
(45, 1, 12, '2022-03-11 21:29:47', '2022-03-11 21:29:47'),
(46, 4, 12, '2022-03-11 21:29:56', '2022-03-11 21:29:56'),
(47, 5, 12, '2022-03-11 21:30:06', '2022-03-11 21:30:06'),
(48, 7, 12, '2022-03-11 21:30:16', '2022-03-11 21:30:16'),
(49, 1, 13, '2022-03-11 21:32:03', '2022-03-11 21:32:03'),
(50, 2, 13, '2022-03-11 21:32:12', '2022-03-11 21:32:12'),
(51, 4, 13, '2022-03-11 21:32:24', '2022-03-11 21:32:24'),
(52, 5, 13, '2022-03-11 21:32:34', '2022-03-11 21:32:34'),
(53, 7, 13, '2022-03-11 21:32:44', '2022-03-11 21:32:44'),
(54, 2, 14, '2022-03-11 21:36:02', '2022-03-11 21:36:02'),
(55, 1, 14, '2022-03-11 21:36:13', '2022-03-11 21:36:13'),
(56, 4, 14, '2022-03-11 21:36:22', '2022-03-11 21:36:22'),
(57, 5, 14, '2022-03-11 21:36:31', '2022-03-11 21:36:31'),
(58, 7, 14, '2022-03-11 21:36:42', '2022-03-11 21:36:42'),
(59, 2, 17, '2022-03-24 20:37:42', '2022-03-24 20:37:42'),
(60, 7, 17, '2022-03-24 20:37:56', '2022-03-24 20:37:56'),
(61, NULL, 8, '2022-03-25 21:27:39', '2022-03-25 21:27:39'),
(62, 5, 8, '2022-03-25 21:27:52', '2022-03-25 21:27:52'),
(63, 2, 18, '2022-03-25 21:33:54', '2022-03-25 21:33:54'),
(64, 7, 18, '2022-03-25 21:34:07', '2022-03-25 21:34:07');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fasilitas`
--

CREATE TABLE `fasilitas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_fasilitas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_fasilitas` enum('Kamar','Hotel') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Kamar',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fasilitas`
--

INSERT INTO `fasilitas` (`id`, `nama_fasilitas`, `icon`, `gambar`, `jenis_fasilitas`, `created_at`, `updated_at`, `status`) VALUES
(2, 'AC', 'icon-1647055052.jpg', 'gambar-1647055052.jpg', 'Kamar', '2022-03-11 20:17:32', '2022-03-11 20:17:32', 1),
(3, 'Kolam Renang', 'icon-1647055087.jpg', 'gambar-1647055087.jpg', 'Hotel', '2022-03-11 20:18:07', '2022-03-11 20:18:07', 1),
(4, 'cermin', 'icon-1647057711.png', 'gambar-1647057711.jpg', 'Kamar', '2022-03-11 21:01:51', '2022-03-11 21:01:51', 1),
(5, 'kursi', 'icon-1647057742.png', 'gambar-1647057742.jpg', 'Kamar', '2022-03-11 21:02:22', '2022-03-11 21:02:22', 1),
(6, 'ruang tunggu', 'icon-1647057848.png', 'gambar-1647057848.jpg', 'Hotel', '2022-03-11 21:04:08', '2022-03-11 21:04:08', 1),
(7, 'kamar mandi', 'icon-1647058800.png', 'gambar-1647058800.jpg', 'Kamar', '2022-03-11 21:20:00', '2022-03-11 21:20:00', 1),
(8, 'parkiran', 'icon-1647060068.png', 'gambar-1647060068.jpg', 'Hotel', '2022-03-11 21:41:08', '2022-03-11 21:41:08', 1),
(9, 'bar hotel', 'icon-1647060184.png', 'gambar-1647060184.jpg', 'Hotel', '2022-03-11 21:43:04', '2022-03-11 21:43:04', 1),
(10, 'Restoran', 'icon-1647060313.png', 'gambar-1647060313.jpg', 'Hotel', '2022-03-11 21:45:13', '2022-03-11 21:45:13', 1),
(11, 'WI-FI', 'icon-1647060453.png', 'gambar-1647060453.jpg', 'Hotel', '2022-03-11 21:47:33', '2022-03-11 21:47:33', 1),
(12, 'gymm', 'icon-1648009858.jpg', 'gambar-1648009858.jpg', 'Hotel', '2022-03-22 21:30:58', '2022-03-25 11:03:04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kamars`
--

CREATE TABLE `kamars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kamar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga` decimal(16,2) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci,
  `tipe_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kamars`
--

INSERT INTO `kamars` (`id`, `nama_kamar`, `harga`, `jumlah`, `deskripsi`, `tipe_id`, `created_at`, `updated_at`, `status`) VALUES
(1, 'kamar vip', '2000000.00', 5, '-', 1, '2022-03-11 20:20:14', '2022-03-24 02:11:45', 0),
(2, 'kamar mawar', '500000.00', 7, '-', 2, '2022-03-11 20:23:01', '2022-03-25 21:32:20', 1),
(3, 'kamar 0102', '1000000.00', 10, '-', 3, '2022-03-11 20:35:30', '2022-03-11 20:35:30', 1),
(4, 'Kamar 01ABC', '1500000.00', 5, '-', 4, '2022-03-11 20:42:54', '2022-03-11 20:42:54', 1),
(5, 'kamar0103', '2000000.00', 5, '-', 5, '2022-03-11 20:51:24', '2022-03-11 21:04:58', 1),
(6, 'kamar 0104', '1700000.00', 7, '-', 6, '2022-03-11 20:53:50', '2022-03-11 20:53:50', 1),
(7, 'kamar 0105', '3000000.00', 6, '-', 7, '2022-03-11 21:07:06', '2022-03-26 22:14:03', 0),
(8, 'kamar 001', '300000.00', 12, '-', 1, '2022-03-11 21:15:23', '2022-03-25 07:32:22', 1),
(9, 'kamar 002', '500000.00', 10, '-', 2, '2022-03-11 21:16:55', '2022-03-11 21:16:55', 1),
(10, 'kamar 003', '1000000.00', 10, '-', 3, '2022-03-11 21:25:07', '2022-03-11 21:25:07', 1),
(11, 'kamar 006', '1500000.00', 5, '-', 4, '2022-03-11 21:26:54', '2022-03-11 21:26:54', 1),
(12, 'kamar 007', '2000000.00', 5, '-', 5, '2022-03-11 21:28:57', '2022-03-11 21:30:44', 1),
(13, 'kamar apartement', '1700000.00', 7, '-', 6, '2022-03-11 21:31:30', '2022-03-11 21:33:09', 1),
(14, 'kamar 009', '3000000.00', 6, '-', 7, '2022-03-11 21:34:52', '2022-03-26 22:14:17', 0),
(15, 'saya', '500000.00', 3, '-', 1, '2022-03-21 20:53:14', '2022-03-24 19:43:42', 0),
(16, 'kamar hijau', '1500000.00', 5, '-', 2, '2022-03-22 21:28:41', '2022-03-24 19:44:47', 0),
(17, 'kamar anggrek', '300000.00', 10, '-', 1, '2022-03-24 20:36:04', '2022-03-25 21:21:11', 1),
(18, 'kamar xx', '300000.00', 7, '-', 1, '2022-03-25 21:33:07', '2022-03-25 21:33:07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kamar_galerys`
--

CREATE TABLE `kamar_galerys` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `kamar_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kamar_galerys`
--

INSERT INTO `kamar_galerys` (`id`, `url`, `foto`, `is_active`, `kamar_id`, `created_at`, `updated_at`) VALUES
(3, NULL, 'galery-1647056434.jpg', 1, 3, '2022-03-11 20:40:34', '2022-03-11 20:40:34'),
(5, NULL, 'galery-1647056530.jpg', 1, 1, '2022-03-11 20:42:10', '2022-03-11 20:42:10'),
(6, NULL, 'galery-1647056706.jpg', 1, 4, '2022-03-11 20:45:06', '2022-03-11 20:45:06'),
(7, NULL, 'galery-1647057160.jpg', 1, 5, '2022-03-11 20:52:40', '2022-03-11 20:52:40'),
(8, NULL, 'galery-1647057369.jpg', 1, 6, '2022-03-11 20:56:09', '2022-03-11 20:56:09'),
(9, NULL, 'galery-1647058147.jpg', 1, 7, '2022-03-11 21:09:07', '2022-03-11 21:09:07'),
(10, NULL, 'galery-1647058295.jpg', 1, 2, '2022-03-11 21:11:35', '2022-03-11 21:11:35'),
(11, NULL, 'galery-1647058555.jpg', 1, 8, '2022-03-11 21:15:55', '2022-03-11 21:15:55'),
(12, NULL, 'galery-1647058638.jpg', 1, 9, '2022-03-11 21:17:18', '2022-03-11 21:17:18'),
(13, NULL, 'galery-1647059134.jpg', 1, 10, '2022-03-11 21:25:34', '2022-03-11 21:25:34'),
(14, NULL, 'galery-1647059260.jpg', 1, 11, '2022-03-11 21:27:40', '2022-03-11 21:27:40'),
(15, NULL, 'galery-1647059366.jpg', 1, 12, '2022-03-11 21:29:26', '2022-03-11 21:29:26'),
(16, NULL, 'galery-1647059513.jpg', 1, 13, '2022-03-11 21:31:53', '2022-03-11 21:31:53'),
(17, NULL, 'galery-1647059752.jpg', 1, 14, '2022-03-11 21:35:52', '2022-03-11 21:35:52'),
(18, NULL, 'galery-1648179447.jpg', 1, 17, '2022-03-24 20:37:27', '2022-03-24 20:37:27'),
(19, NULL, 'galery-1648269224.jpg', 1, 18, '2022-03-25 21:33:44', '2022-03-25 21:33:44');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_03_07_163846_create_table_kamars', 2),
(6, '2022_03_07_164239_create_table_detail_fasilitas_kamars', 3),
(7, '2022_03_07_164416_create_fasilitas', 4),
(8, '2022_03_07_164826_create_kamar_galerys', 5),
(9, '2022_03_07_165312_create_reservasi', 6),
(10, '2022_03_07_165727_create_reviews', 7),
(11, '2022_03_07_165904_create_tamu', 8),
(12, '2022_03_07_170153_create_tipe_kamars', 9),
(13, '2022_03_11_004037_alter_tamus', 10),
(14, '2022_03_11_004458_alter_users', 10),
(15, '2022_03_24_040655_alter_table_kamars', 11),
(16, '2022_03_24_043902_alter_fasilitas', 11);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('retnaargapura1@gmail.com', '$2y$10$6SfZFIsGHvEPP2lwWlveFuROqFYx1Yett9CjYxNH1zJ57OBT2lpGa', '2022-03-07 08:02:36');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reservasis`
--

CREATE TABLE `reservasis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `lama` int(11) DEFAULT NULL,
  `qty_kamar` int(11) DEFAULT NULL,
  `status` enum('RESERVASI','CHECKIN','CHECKOUT','CANCEL') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `canceled_by` int(11) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `canceled_date` datetime DEFAULT NULL,
  `tamu_id` int(11) DEFAULT NULL,
  `kamar_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reservasis`
--

INSERT INTO `reservasis` (`id`, `start_date`, `end_date`, `lama`, `qty_kamar`, `status`, `approved_by`, `canceled_by`, `approved_date`, `canceled_date`, `tamu_id`, `kamar_id`, `created_at`, `updated_at`) VALUES
(1, '2022-03-12', '2022-03-15', 1, 2, 'CHECKOUT', 3, NULL, '2022-03-12 03:34:17', NULL, 1, 1, '2022-03-11 20:31:36', '2022-03-11 20:34:23'),
(2, '2022-03-12', '2022-03-13', 1, 1, 'RESERVASI', NULL, 3, NULL, '2022-03-12 03:49:42', 1, 2, '2022-03-11 20:47:20', '2022-03-11 20:49:42'),
(3, '2022-03-12', '2022-03-19', 1, 1, 'RESERVASI', NULL, 3, NULL, '2022-03-21 04:12:01', 6, 11, '2022-03-11 22:18:42', '2022-03-20 21:12:01'),
(4, '2022-03-15', '2022-03-16', 1, NULL, 'RESERVASI', NULL, 3, NULL, '2022-03-12 05:21:30', 7, 9, '2022-03-11 22:19:54', '2022-03-11 22:21:30'),
(5, '2022-03-14', '2022-03-15', 1, 1, 'CHECKOUT', 3, NULL, '2022-03-12 05:21:35', NULL, 5, 14, '2022-03-11 22:20:42', '2022-03-20 21:11:51'),
(6, '2022-03-16', '2022-03-17', 2, 1, 'CHECKOUT', 3, NULL, '2022-03-16 02:46:44', NULL, 4, 1, '2022-03-15 16:26:43', '2022-03-20 21:11:42'),
(7, '2022-03-16', '2022-03-17', 2, 1, 'CHECKOUT', 3, NULL, '2022-03-16 02:45:23', NULL, 4, 1, '2022-03-15 16:26:44', '2022-03-20 21:11:33'),
(8, '2022-03-16', '2022-03-17', 2, NULL, 'CHECKOUT', 3, NULL, '2022-03-19 03:10:44', NULL, 6, 11, '2022-03-15 19:56:10', '2022-03-20 21:11:25'),
(9, '2022-03-16', '2022-03-17', 2, 1, 'CHECKOUT', 3, NULL, '2022-03-19 03:03:43', NULL, 6, 1, '2022-03-15 19:57:41', '2022-03-18 20:04:32'),
(10, '2022-03-19', '2022-03-20', 2, 1, 'CHECKOUT', 3, NULL, '2022-03-21 04:11:08', NULL, 7, 10, '2022-03-18 20:43:22', '2022-03-20 21:11:15'),
(11, '2022-03-20', '2022-03-21', 2, 3, 'RESERVASI', NULL, 3, NULL, '2022-03-21 04:11:02', 8, 8, '2022-03-20 02:09:37', '2022-03-20 21:11:02'),
(12, '2022-03-21', '2022-03-22', 2, 1, 'CHECKOUT', 3, NULL, '2022-03-21 04:13:34', NULL, 8, 8, '2022-03-20 21:13:01', '2022-03-24 02:12:09'),
(13, '2022-03-21', '2022-03-22', 2, 1, 'CHECKOUT', 3, NULL, '2022-03-21 12:19:00', NULL, 6, 8, '2022-03-21 05:18:32', '2022-03-24 02:12:02'),
(14, '2022-03-22', '2022-03-23', 2, 2, 'CHECKOUT', 3, NULL, '2022-03-22 04:02:00', NULL, 9, 8, '2022-03-21 20:59:38', '2022-03-21 21:02:13'),
(15, '2022-03-23', '2022-03-24', 2, 2, 'CHECKOUT', 3, NULL, '2022-03-23 04:33:58', NULL, 4, 13, '2022-03-22 21:32:34', '2022-03-22 21:34:08'),
(16, '2022-03-24', '2022-03-25', 2, 1, 'CHECKOUT', 3, NULL, '2022-03-25 14:32:15', NULL, 7, 8, '2022-03-23 20:54:57', '2022-03-25 07:32:21'),
(17, '2022-03-24', '2022-03-25', 2, 1, 'CHECKOUT', 3, NULL, '2022-03-24 05:57:49', NULL, 7, 1, '2022-03-23 22:57:16', '2022-03-24 02:11:45'),
(18, '2022-03-24', '2022-03-25', 2, 1, 'CHECKOUT', 3, NULL, '2022-03-24 09:13:55', NULL, 4, 2, '2022-03-24 02:13:15', '2022-03-25 07:32:07'),
(19, '2022-03-24', '2022-03-25', 2, 1, 'CHECKOUT', 3, NULL, '2022-03-24 09:17:07', NULL, 6, 2, '2022-03-24 02:16:38', '2022-03-24 02:17:51'),
(20, '2022-03-26', '2022-03-30', 5, 1, 'CHECKOUT', 3, NULL, '2022-03-26 04:21:03', NULL, 5, 17, '2022-03-25 21:17:52', '2022-03-25 21:21:11'),
(21, NULL, NULL, 1, NULL, 'RESERVASI', NULL, NULL, NULL, NULL, 6, 8, '2022-03-26 23:12:40', '2022-03-26 23:12:40'),
(22, NULL, NULL, 1, NULL, 'RESERVASI', NULL, NULL, NULL, NULL, 6, 13, '2022-03-26 23:22:50', '2022-03-26 23:22:50');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci,
  `star` int(11) NOT NULL DEFAULT '0',
  `tamu_id` int(11) DEFAULT NULL,
  `kamar_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `review`, `star`, `tamu_id`, `kamar_id`, `created_at`, `updated_at`) VALUES
(1, 'baik sekali', 5, 4, 1, '2022-03-23 21:01:17', '2022-03-23 21:01:17'),
(2, 'baik sekali', 5, 6, 11, '2022-03-23 21:06:10', '2022-03-23 21:06:10'),
(3, 'baik sekali', 5, 4, 13, '2022-03-23 23:46:39', '2022-03-23 23:46:39'),
(4, 'bagus seklai', 4, 7, 10, '2022-03-24 19:57:39', '2022-03-24 19:57:39'),
(5, 'sangat bagus', 5, 7, 1, '2022-03-24 19:57:51', '2022-03-24 19:57:51'),
(6, 'baik', 5, 6, 1, '2022-03-24 20:27:08', '2022-03-24 20:27:08'),
(7, 'mantep', 4, 6, 8, '2022-03-24 20:27:20', '2022-03-24 20:27:20'),
(8, 'good sekali', 4, 6, 2, '2022-03-24 20:27:31', '2022-03-24 20:27:31'),
(9, 'Baik sekali', 5, 7, 8, '2022-03-26 00:30:27', '2022-03-26 00:30:27');

-- --------------------------------------------------------

--
-- Table structure for table `tamus`
--

CREATE TABLE `tamus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nik` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jeni_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telepon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `jenis_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tamus`
--

INSERT INTO `tamus` (`id`, `nik`, `nama`, `jeni_kelamin`, `alamat`, `telepon`, `user_id`, `created_at`, `updated_at`, `jenis_kelamin`) VALUES
(1, '143452613745', 'aep mulyadi', NULL, 'pasirkunyit', '0857900324302', '2', '2022-03-11 20:24:37', '2022-03-27 13:01:51', 'laki-laki'),
(2, '243516276513624', 'aefmulyadi', NULL, 'pasirkunyit', '085790324303', '3', '2022-03-11 20:26:46', '2022-03-11 20:26:46', 'laki-laki'),
(3, '1223455', 'tamu 1', NULL, 'tempat', '0929324823641', '4', '2022-03-11 22:04:08', '2022-03-11 22:04:08', 'perempuan'),
(4, '124312432163575', 'tamu2', NULL, 'tempat anda', '098243472364', '5', '2022-03-11 22:08:49', '2022-03-11 22:08:49', 'perempuan'),
(5, '12536422167', 'tamu 3', NULL, 'pasirkunyit', '012834765332', '6', '2022-03-11 22:10:10', '2022-03-11 22:10:10', 'laki-laki'),
(6, '1326425423726', 'tamu 4', NULL, 'cilacap', '0849347471612', '7', '2022-03-11 22:11:27', '2022-03-11 22:11:27', 'perempuan'),
(7, '900982748657326', 'tamu6', NULL, 'cilacap', '0104883462375', '8', '2022-03-11 22:14:32', '2022-03-11 22:14:32', 'laki-laki'),
(8, '355119637327', 'tamu5', NULL, 'cilacap', '085790324302', '9', '2022-03-20 02:08:42', '2022-03-20 02:08:42', 'laki-laki'),
(9, '12534221652', 'tamu10', NULL, 'cilacap', '03475546152171', '10', '2022-03-21 20:59:08', '2022-03-21 20:59:08', 'laki-laki');

-- --------------------------------------------------------

--
-- Table structure for table `tipe_kamars`
--

CREATE TABLE `tipe_kamars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipe_kamar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tipe_kamars`
--

INSERT INTO `tipe_kamars` (`id`, `tipe_kamar`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Standart Room', 1, '2022-03-11 20:09:19', '2022-03-11 20:09:19'),
(2, 'Deluxe Room', 1, '2022-03-11 20:09:39', '2022-03-11 20:09:39'),
(3, 'Double-Double Room', 1, '2022-03-11 20:09:58', '2022-03-11 20:09:58'),
(4, 'Interconnecting Room', 1, '2022-03-11 20:10:17', '2022-03-11 20:10:17'),
(5, 'Suite', 1, '2022-03-11 20:10:36', '2022-03-11 20:10:36'),
(6, 'Apartment-style', 1, '2022-03-11 20:10:51', '2022-03-11 20:10:51'),
(7, 'Accessible Room', 0, '2022-03-11 20:11:08', '2022-03-26 22:14:28'),
(8, 'vip', 0, '2022-03-21 20:54:19', '2022-03-26 22:13:05'),
(9, 'bagus', 0, '2022-03-22 21:29:23', '2022-03-26 22:12:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` enum('Admin','Reception','Visit') COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role`) VALUES
(1, 'retnasuhada', 'retnaargapura1@gmail.com', NULL, '$2y$10$amduhuKvwN.QB/WiEGgeKeo32Hlo6i1WUd4ajdbtms3Fc/5p3kEj6', NULL, '2022-03-07 08:01:42', '2022-03-07 08:01:42', 'Admin'),
(2, 'aep mulyadi', 'aepmulyadi19@gmail.com', NULL, '$2y$10$7b2Y/CPzlXRCX0.Gzgeb2ee9t0pxhp2xSgCt.0Ti5fh5B3ri/Ddp2', NULL, '2022-03-11 20:24:37', '2022-03-11 22:15:48', 'Admin'),
(3, 'aefmulyadi', 'aepmulyadi28@gmail.com', NULL, '$2y$10$UH6ZWPojMKSq9lYmGgnidu9q/8.WJpLu14NqW9Nir//q3tu98reV.', NULL, '2022-03-11 20:26:46', '2022-03-11 20:27:55', 'Reception'),
(4, 'tamu 1', 'tamu1@gmail.com', NULL, '$2y$10$wH5mb3EZMXmSYp8pB3gFU.IWwLV9va40Azvdotd3bRLUdu1idAZzu', NULL, '2022-03-11 22:04:08', '2022-03-11 22:15:36', 'Visit'),
(5, 'tamu2', 'tamu2@gmail.com', NULL, '$2y$10$Mb9E9otcdFjF.w.W0l7WWu0v8ifUgt6.mfWu1wNYINg7uGF8Px2Gy', NULL, '2022-03-11 22:08:48', '2022-03-11 22:08:48', 'Visit'),
(6, 'tamu 3', 'tamu3@gmail.com', NULL, '$2y$10$9aV.qdba6XDGvVOSvgRCO.IIwiYyaonae/Xep41jZpOEWkZRc2Qv.', NULL, '2022-03-11 22:10:10', '2022-03-11 22:10:10', 'Visit'),
(7, 'tamu 4', 'tamu4@gmail.com', NULL, '$2y$10$rBp1tAfv/Lk/5veGml0QO.FJHdM.5l1.epkIcRj42iQSnVc2N8yAS', NULL, '2022-03-11 22:11:27', '2022-03-11 22:11:27', 'Visit'),
(8, 'tamu6', 'tamu6@gmail.com', NULL, '$2y$10$Ne/48pUmDtL/1ANxFPYECuea4RYqmTEsPgEyp/TUIjPptkvQdmcoC', NULL, '2022-03-11 22:14:32', '2022-03-11 22:14:32', 'Visit'),
(9, 'tamu5', 'tamu5@gmail.com', NULL, '$2y$10$K905hI3ehrGs/T.5HqeZF.TBvt21u0Oc1pI6t0/WB5naVy39LY1wG', NULL, '2022-03-20 02:08:42', '2022-03-20 02:08:42', 'Visit'),
(10, 'tamu10', 'tamu10@gmail.com', NULL, '$2y$10$NlDHRNjueYQCgnTEXvSPFOdX.zuIKsBGrNBt4bGGAhdqvtRAREU2G', NULL, '2022-03-21 20:59:08', '2022-03-21 20:59:08', 'Visit');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_fasilitas_kamars`
--
ALTER TABLE `detail_fasilitas_kamars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `fasilitas`
--
ALTER TABLE `fasilitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kamars`
--
ALTER TABLE `kamars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kamar_galerys`
--
ALTER TABLE `kamar_galerys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `reservasis`
--
ALTER TABLE `reservasis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tamus`
--
ALTER TABLE `tamus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipe_kamars`
--
ALTER TABLE `tipe_kamars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_fasilitas_kamars`
--
ALTER TABLE `detail_fasilitas_kamars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fasilitas`
--
ALTER TABLE `fasilitas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `kamars`
--
ALTER TABLE `kamars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `kamar_galerys`
--
ALTER TABLE `kamar_galerys`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reservasis`
--
ALTER TABLE `reservasis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tamus`
--
ALTER TABLE `tamus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tipe_kamars`
--
ALTER TABLE `tipe_kamars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
