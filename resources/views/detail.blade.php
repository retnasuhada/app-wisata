@extends('layouts.app-tamu')
@section('content')
<!--================ Accomodation Area  =================-->


<!--================ Facilities Area  =================-->
<section class="facilities_area section_gap">
    <div class="overlay bg-parallax" data-stellar-ratio="0.8" data-stellar-vertical-offset="0" data-background="">
    </div>
    <div class="container">
        <div class="section_title text-center">
            <h2 class="title_w">Fasilitas</h2>
            <p>Fasilitas Yang Tersedia</p>
        </div>
        <div class="row mb_30">
            @foreach($data as $row)
            <div class="col-lg-4 col-md-6">
                <div class="facilities_item">
                    <h4 class="sec_h4"><i class="lnr lnr-dinner"></i>{{$row->fasilitas->nama}}</h4>
                    <p>{{$row->fasilitas->deskripsi}}.</p>
                </div>
            </div>
            @endforeach


        </div>
    </div>
</section>
<!--================ Facilities Area  =================-->
<section class="about_history_area section_gap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 d_flex align-items-center">
                <div class="about_content ">
                    {{-- <h2 class="title title_color">{{$data['kamar']['nama_kamar']}}</h2> --}}
                    {{-- {{$data['kamar']['foto']}} --}}
                </div>
            </div>

        </div>
    </div>
</section>
<!--================ About History Area  =================-->
<br />
<!--================Booking Tabel Area =================-->
<section class="hotel_booking_area">
    <div class="container">
        <div class="row hotel_booking_table">
            <div class="col-md-6">
                <h2>Jadwal Keberangkatan<br></h2>
            </div>

            <div class="col-md-12">
                <div class="boking_table">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="book_tabel_item">
                                <div class="form-group">
                                    <div class='input-group date'>
                                        <h5>Tanggal</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="book_tabel_item">
                                <div class="form-group">
                                    <div class='input-group date'>
                                        <h5>Titik Kumpul</h5>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="book_tabel_item">
                                <div class="form-group">
                                    <div class='input-group date'>
                                        <h5>Waktu Keberangkatan</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="book_tabel_item">
                                {{-- <button type="submit" class="book_now_btn button_hover"> BOOKING</button> --}}
                            </div>
                        </div>
                    </div>

                    @foreach($jadwal as $rowJadwal)
                    <div class="row">
                        <div class="col-md-3">
                            <div class="book_tabel_item">
                                <div class="form-group">
                                    <div class='input-group date'>
                                        <h5>{{$rowJadwal->tanggal}}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="book_tabel_item">
                                <div class="form-group">
                                    <div class='input-group date'>
                                        <h5>{{$rowJadwal->start_location}}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="book_tabel_item">
                                <div class="form-group">
                                    <div class='input-group date'>
                                        <h5>{{$rowJadwal->start_time}}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="book_tabel_item">
                                <a href="#" class="book_now_btn button_hover" onClick="hapus({{$rowJadwal->id}})" data-toggle="modal" data-target="#modal-danger">
                                    BOOKING
                                </a>
                                {{-- <a href="{{route('umum.booking',$rowJadwal->id)}}" class="book_now_btn button_hover"> BOOKING</a> --}}
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-danger">
        <div class="modal-dialog">
            <form class="form-horizontal" action="{{route('umum.booking.store')}}" method="POST">
                @csrf
                <div class="modal-content bg-primary">
                    <div class="modal-header">
                        <h4 class="modal-title">Silahkan masukan jumlah yang ingin anda pesan</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="modal-body">
                         <input type="number" class="form-control" name="jumlah" id="inputEmail3" placeholder="Masukan Jumlah Pesanan Anda">

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-outline-light">Simpan</button>
                    </div>
                </div>
                <!-- /.modal-content -->
        </div>
        </form>
        <!-- /.modal-dialog -->
    </div>
</section>
<!--================Booking Tabel Area  =================-->
{{-- <!--================ Testimonial Area  =================-->
<section class="testimonial_area section_gap">
    <div class="container">
        <div class="section_title text-center">
            <h2 class="title_color">Testimonial Kamar Ini</h2>
        </div>
        <div class="testimonial_slider owl-carousel"> --}}
{{-- @foreach($data['review'] as $row)
            <div class="media testimonial_item">
                <img class="rounded-circle" src="image/testtimonial-1.jpg" alt="">
                <div class="media-body">
                    <p>{{$row['review']}}</p>
<a href="#">
    <h4 class="sec_h4">{{$row['tamu']['nama']}}</h4>
</a>
@if($row['star']=='1')
<div class="star">
    <a href="#"><i class="fa fa-star"></i></a>
    <a href="#"><i class="fa fa-star-o"></i></a>
    <a href="#"><i class="fa fa-star-o"></i></a>
    <a href="#"><i class="fa fa-star-o"></i></a>
    <a href="#"><i class="fa fa-star-o"></i></a>
</div>
@endif
@if($row['star']=='2')
<div class="star">
    <a href="#"><i class="fa fa-star"></i></a>
    <a href="#"><i class="fa fa-star"></i></a>
    <a href="#"><i class="fa fa-star-o"></i></a>
    <a href="#"><i class="fa fa-star-o"></i></a>
    <a href="#"><i class="fa fa-star-o"></i></a>
</div>
@endif
@if($row['star']=='3')
<div class="star">
    <a href="#"><i class="fa fa-star"></i></a>
    <a href="#"><i class="fa fa-star"></i></a>
    <a href="#"><i class="fa fa-star"></i></a>
    <a href="#"><i class="fa fa-star-o"></i></a>
    <a href="#"><i class="fa fa-star-o"></i></a>
</div>
@endif
@if($row['star']=='4')
<div class="star">
    <a href="#"><i class="fa fa-star"></i></a>
    <a href="#"><i class="fa fa-star"></i></a>
    <a href="#"><i class="fa fa-star"></i></a>
    <a href="#"><i class="fa fa-star"></i></a>
    <a href="#"><i class="fa fa-star-o"></i></a>
</div>
@endif
@if($row['star']=='5')
<div class="star">
    <a href="#"><i class="fa fa-star"></i></a>
    <a href="#"><i class="fa fa-star"></i></a>
    <a href="#"><i class="fa fa-star"></i></a>
    <a href="#"><i class="fa fa-star"></i></a>
    <a href="#"><i class="fa fa-star"></i></a>
</div>
@endif

</div>
</div>
@endforeach --}}

{{-- </div>
    </div>
</section> --}}
<!--================ Accomodation Area  =================-->
<script>
    function hapus(id) {

        let html = `<input type="hidden" name="jadwal_id" value="${id}"/>`;
        $('#modal-body').append(html);

    }

</script>
@endsection
