@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">@yield('content-title')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Starter Page</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-info">
                    <!-- /.card-header -->
                    <!-- form start -->
                    @if($errors->any())
                    <h5 style="color:red;">{{$errors->first()}}</h5>
                    @endif
                    <div class="card-body">
                <p class="login-box-msg">Edit Data Diri Anda</p>

                <form method="POST" enctype="multipart/form-data" action="{{ route('profil.update') }}">

                    @csrf
                    <input type="hidden" name="role" value="1">
                    <div class="row mb-3">
                        <label for="nik" class="col-md-4 col-form-label text-md-end">{{ __('NIK') }}</label>

                        <div class="col-md-8">
                            <input id="nik" type="text" class="form-control" name="nik" required value={{$data->nik}} autocomplete="nik" autofocus>

                            @error('nik')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="nama" class="col-md-4 col-form-label text-md-end">{{ __('Nama') }}</label>

                        <div class="col-md-8">
                            <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" value="{{$data->nama}}" name="nama" required autocomplete="nama" autofocus>

                            @error('nama')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="jenis_kelamin" class="col-md-4 col-form-label text-md-end">{{ __('Gender') }}</label>

                        <div class="col-md-8">
                            <select name="jenis_kelamin" class="form-control">
                                <option value="">-Pilih Jenis Kelamin-</option>
                                <option value="laki-laki" <?php if($data->jenis_kelamin=='laki-laki'){echo "selected";} ?>>Laki-Laki</option>
                                <option value="perempuan" <?php if($data->jenis_kelamin=='perempuan'){echo "selected";} ?>>Perempuan</option>
                            </select>
                            @error('jenis_kelamin')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="alamat" class="col-md-4 col-form-label text-md-end">{{ __('Alamat') }}</label>

                        <div class="col-md-8">
                            <textarea name="alamat" class="form-control">{{$data->alamat}}</textarea>

                            @error('alamat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="telepon" class="col-md-4 col-form-label text-md-end">{{ __('telepon') }}</label>

                        <div class="col-md-8">
                            <input id="phone" type="text" class="form-control @error('telepon') is-invalid @enderror" name="telepon" value="{{$data->telepon}}" required autocomplete="phone" autofocus>

                            @error('telepon')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>


                    <div class="row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Simpan') }}
                            </button>
                        </div>
                    </div>
                </form>

            </div>

                </div>
            </div>

        </div>
    </div>
</div>

</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>

@endsection
