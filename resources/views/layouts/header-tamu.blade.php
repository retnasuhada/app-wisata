<!--================Header Area =================-->
<header class="header_area">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <!-- Brand and toggle get grouped for better mobile display -->
            <a class="navbar-brand logo_h" href="index.html"><img src="{{ asset('royal/image/Logo-new.png')}}" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                <ul class="nav navbar-nav menu_nav ml-auto">
                    <li class="nav-item active"><a class="nav-link" href="{{route('index')}}">Home</a></li>


                    @if(!Auth::check())
                    <li class="nav-item"><a class="nav-link" href="{{url('login')}}">Login</a></li>
                    @else
                    <li class="nav-item"><a class="nav-link" href="{{url('tamu/pesanan')}}">Pesanan Saya</a></li>

                    <li class="nav-item">
                        <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">

                            Logout


                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="{{route('profil')}}">Profil</a></li>
                    @endif

                </ul>
            </div>
        </nav>
    </div>
</header>
<!--================Header Area =================-->
