@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">@yield('content-title')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Jadwal</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List Jadwal Tour</h3>
                        <a href="{{route('admin.jadwal.add')}}" class="btn btn-success float-right">Tambah</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Wisata</th>
                                    <th>Titik Kumpul</th>
                                    <th>Waktu Pemberangkatan</th>
                                    <th>Min Peserta</th>
                                    <th>Max Peserta</th>
                                    <th>Jumlah Booking</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 0;
                                ?>
                                @foreach($data as $row)
                                <?php
                                $no++;
                                ?>
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$row->tanggal}}</td>
                                    <td>{{$row->wisata->nama}}</td>
                                    <td>{{$row->start_location}}</td>
                                    <td>{{$row->start_time}}</td>
                                    <td>{{$row->min_peserta}}</td>
                                    <td>{{$row->max_peserta}}</td>
                                    <td>
                                        @php
                                        $booking = App\Models\Booking::query()->where(['jadwal_id' => $row->id, 'status'=>'terbayar'])->sum('jumlah');
                                        @endphp
                                        {{$booking}}
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/jadwal/show',$row['id']) }}">
                                            <i class="nav-icon fas fa-search-plus"></i>
                                        </a>
                                        &nbsp;&nbsp;
                                        <a href="{{ url('admin/jadwal/edit',$row['id']) }}">
                                            <i class="nav-icon fas fa-pencil-alt"></i>
                                        </a>

                                        &nbsp;&nbsp;

                                        <a href="#" onClick="hapus({{$row['id']}})" data-toggle="modal" data-target="#modal-danger">
                                            <i class="nav-icon fas fa-trash-alt"></i>
                                        </a>

                                    </td>
                                </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="modal fade" id="modal-danger">
                <div class="modal-dialog">
                    <form class="form-horizontal" action="#" method="POST">
                        @csrf
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Apakah Data Ini Akan Di Hapus?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div id="modal-body">

                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
                                <button type="submit" class="btn btn-outline-light">Ya</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                </div>
                </form>
                <!-- /.modal-dialog -->
            </div>
        </div>
    </div>
</div>

</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>
<script>
    function hapus(id) {

        let html = `<input type="hidden" name="id" value="${id}"/>`;
        $('#modal-body').append(html);

    }

</script>
@endsection
