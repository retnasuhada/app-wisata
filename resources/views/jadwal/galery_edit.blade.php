@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">@yield('content-title')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Starter Page</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-info">
                    <!-- /.card-header -->
                    <!-- form start -->
                    @if(Session::has('error'))
                    <input type="hidden" value="dupplicate" id="success">
                    @endif
                    <form class="form-horizontal" action="{{ route('kamar.galery.store') }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Nama Kamar</label>
                                <div class="col-sm-9">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">{{$data['kamar']['nama_kamar']}}</label>
                                </div>
                            </div>
                            <input type="hidden" name="kamar_id" value="{{$data['kamar']['id']}}">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Galery</label>
                                <div class="col-sm-9">
                                    <div class="custom-file">
                                        <input type="file" name="galery" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    @if ($errors->has('galery'))
                                    <span class="text-danger">{{ $errors->first('galery') }}</span>
                                    @endif
                                </div>
                            </div>

                            

                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Tambahkan</button>
                            <a href="{{ url('kamar',$data['kamar']['id']) }}" class="btn btn-warning">Kembali</a>
                        </div>
                        <!-- /.card-footer -->
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>

</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>

@endsection