@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">@yield('content-title')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Jadwal / Tambah</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-info">
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{ route('admin.jadwal.store') }}" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Nama Wisata</label>
                                <div class="col-sm-9">
                                    <select name="wisata_id" class="form-control">
                                    <option value="">-Pilih-</option>
                                    @foreach($data as $row)
                                        <option value="{{$row->id}}">{{$row->nama}}</option>
                                    @endforeach
                                    </select>
                                    @if ($errors->has('wisata_id'))
                                    <span class="text-danger">{{ $errors->first('wisata_id') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Tanggal Keberangkatan</label>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" name="tanggal" id="inputEmail3">
                                    @if ($errors->has('tanggal'))
                                    <span class="text-danger">{{ $errors->first('tanggal') }}</span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Titik Kumpul Keberangkatan</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="start_location" id="inputEmail3" >
                                    @if ($errors->has('start_location'))
                                    <span class="text-danger">{{ $errors->first('start_location') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Jam Keberangkatan</label>
                                <div class="col-sm-9">
                                    <input type="time" class="form-control" name="start_time" id="inputEmail3">
                                    @if ($errors->has('start_time'))
                                    <span class="text-danger">{{ $errors->first('start_time') }}</span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Jumlah Minimal Peserta</label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" name="min_peserta" id="inputEmail3" >
                                    @if ($errors->has('min_peserta'))
                                    <span class="text-danger">{{ $errors->first('min_peserta') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Jumlah Maksimal Peserta</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="max_peserta" id="inputEmail3" >
                                    @if ($errors->has('max_peserta'))
                                    <span class="text-danger">{{ $errors->first('max_peserta') }}</span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Simpan</button>
                            <a href="{{url('admin/jadwal')}}" class="btn btn-warning">Kembali</a>

                            <button type="reset" class="btn btn-danger ">Batal</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>

</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>

@endsection
