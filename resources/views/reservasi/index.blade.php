@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">@yield('content-title')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Starter Page</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List Reservasi</h3>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tamu</th>
                                    <th>Tipe</th>
                                    <th>Kamar</th>
                                    <th>Jumlah</th>
                                    <th>Chek-In</th>
                                    <th>Chek-Out</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no = 0;
                                @endphp
                                @foreach($data as $key =>$value)
                                @php
                                $no++;
                                @endphp
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$value['tamu']['nama']}}</td>
                                    <td>{{$value['kamar']['tipeKamar']['tipe_kamar']}}</td>
                                    <td>{{$value['kamar']['nama_kamar']}}</td>
                                    <td>{{$value['qty_kamar']}}</td>
                                    <td>{{$value['start_date']}}</td>
                                    <td>{{$value['end_date']}}</td>
                                    <td>
                                        @if($value['canceled_by']==null)
                                            @if($value['status']=='RESERVASI')
                                            Menunggu Verifikasi
                                            
                                            @elseif($value['status']=='CHECKIN')
                                            Chek-In
                                            @else
                                            Chek-Out
                                            @endif
                                        @else
                                        <p style="font-size:16px; color:red">Ditolak</p>
                                        @endif


                                    </td>
                                    <td>
                                    <a href="{{ url('reservasi',$value['id']) }}">
                                            <i class="nav-icon fas fa-search-plus"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>
    </div>
</div>

</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>

@endsection
