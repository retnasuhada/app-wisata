@extends('layouts.app-tamu')
@section('content')
<!--================ Accomodation Area  =================-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style type="text/css">
    table tr td,
    table tr th {
        font-size: 9pt;
    }

</style>

<!--================ Facilities Area  =================-->
<section class="facilities_area section_gap">
    <section class="button-area">
        <div class="container border-top-generic">
        <p>Untuk pembayaran silahan transfer ke : BCA - Atas Nama  PT. DESA PANIIS dengan nomor Rekening <b>1234567890</b></p>
            <h3 class="text-heading title_color">Data Pesanan Anda</h3>
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Wisata</th>
                                <th>Tanggal Berangkat</th>
                                <th>Waktu Berangkat</th>
                                <th>Harga</th>
                                <th>Jumlah</th>
                                <th>Subtotal</th>
                                <th>status</th>
                                <th></th>

                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $no=0;
                            @endphp


                            @foreach($data as $row)
                            @php
                            $no++;
                            @endphp
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$row->jadwal->wisata->nama}}</td>
                                <td>{{$row->jadwal->tanggal}}</td>
                                <td>{{$row->jadwal->start_time}}</td>
                                <td>{{"Rp. ".number_format($row->jadwal->wisata->harga)}}</td>
                                <td>{{$row->jumlah}}</td>
                                <td>{{"Rp. ".number_format($row->jumlah * $row->jadwal->wisata->harga)}}</td>
                                <td>
                                    @if($row->status=='none')
                                    Belum Bayar
                                    @elseif($row->status=='pending')
                                    Sedang Di Verifikasi
                                    @else
                                    Lunas
                                    @endif
                                </td>

                                <td>
                                    @if($row->status=='none')
                                    <a href="#" onClick="hapus({{$row->id}})" data-toggle="modal" data-target="#myModal" class="btn btn-primary">Bayar</a>
                                    <a href="#" class="btn btn-danger">Batalkan</a>
                                    @else
                                    <a href="#" class="btn btn-primary">Detail</a>
                                    @endif
                                </td>




                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
        </div>


    </section>


</section>
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-top">
        <form class="form theme-form" action="{{route('umum.booking.buy')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="topModalLabel">Upload Bukti Pembayaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <label for="product-name">Bukti Pembayaran<span class="text-danger">*</span></label>
                    <input type="file" name="bukti" required id="bukti" class="form-control">

                     <label for="product-name">Jumlah Transfer<span class="text-danger">*</span></label>
                    <input type="number" name="jumlah" required id="jumlah" class="form-control">

                     <label for="product-name">Nama Bank Pengirim<span class="text-danger">*</span></label>
                    <input type="text" name="nm_bank_pengirim" required id="nm_bank_pengirim" class="form-control">

                    <label for="product-name">Nama Pemilik Rekening<span class="text-danger">*</span></label>
                    <input type="text" name="nm_pemilik_rekening" required id="nm_pemilik_rekening" class="form-control">


                </div>
                <div class="dropzone-previews mt-6">
                    <div id="image-previews"> </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Simpan</button>
                </div>
            </div>
    </div>
    </form>
</div>
<script>
    function hapus(id) {

        let html = `<input type="hidden" name="booking_id" value="${id}"/>`;
        $('.modal-body').append(html);

    }

</script>
<!--================ Accomodation Area  =================-->
@endsection
