@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Fasilitas</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Fasilitas | Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">



                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    <i class="fas fa-archway"></i> Detail Fasilitas

                                </h4>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <div class="col-sm-3 invoice-col">

                                <address>
                                    <strong>Nama Fasilitas</strong><br>
                                    Deskripsi<br>
                                    Status
                                </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-9 invoice-col">

                                <address>
                                    <strong>{{$fasilitas->nama}}</strong><br>
                                    {{$fasilitas->deskripsi}}<br>
                                    {{$fasilitas->status}}<br>

                                </address>
                            </div>
                            <!-- /.col -->

                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <!-- Table row -->
                        <div class="row justify-content-center">
                            <div class="col-md-12">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title">Galery</h3>
                                        {{-- <a href="{{ url('kamar/galery/add',$fasilitas->id) }}" class="btn btn-warning btn-sm float-right">Tambah</a> --}}
                                        <a href="{{ url('fasilitas/galeri/add',$fasilitas->id) }}" class="btn  btn-warning btn-sm float-right">Tambah</a>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table id="example2" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="5%">No</th>
                                                    <th>Gambar</th>
                                                    <th>Deskripsi</th>
                                                    <th>Foto Utama</th>
                                                    <th width="10%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $no=0;
                                                @endphp
                                                @foreach($galeri as $rows)
                                                @php
                                                $no++;
                                                @endphp
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>
                                                        <img style="width: 150px; height:150px;" src="{{asset('images/'.$rows['foto'])}}">
                                                    </td>
                                                    <td>{{$rows->deskripsi}}</td>
                                                    <td>
                                                    @if($rows->is_main=='1')
                                                    Foto Utama
                                                    @else
                                                    <a href="{{url('fasilitas/galeri/setMain/'.$rows->id,$fasilitas->id)}}" class="btn  btn-warning btn-sm">Jaidkan Foto Utama</a>
                                                    @endif
                                                    </td>
                                                    <td>

                                                        <a href="{{ url('fasilitas/galeri/delete',$rows->id) }}">
                                                            <i class="nav-icon fas fa-trash-alt"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                @endforeach

                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>


                        </div>

                        <!-- /.row -->


                    </div>
                    <!-- /.invoice -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>

@endsection
