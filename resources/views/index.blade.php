@extends('layouts.app-tamu')
@section('content')
<!--================ Accomodation Area  =================-->
<section class="breadcrumb_area">
    <div class="overlay bg-parallax" data-stellar-ratio="0.8" data-stellar-vertical-offset="0" data-background=""></div>
    <div class="container">
        <div class="page-cover text-center">
            <h2 class="page-cover-tittle">Wisata</h2>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li class="active">Wisata</li>
            </ol>
        </div>
    </div>
</section>

<section class="accomodation_area section_gap">
    <div class="container">
        <div class="section_title text-center">
            <h2 class="title_color">Pilihan Wista Terbaik Untuk Anda</h2>
            {{-- <p>We all live in an age that belongs to the young at heart. Life that is becoming extremely fast,</p> --}}
        </div>
        <div class="row mb_30">
            @foreach($data as $row)
            <div class="col-lg-3 col-sm-6">
                <div class="accomodation_item text-center">
                    <div class="hotel_img">
                    @php
                        $image = App\Models\GaleriWisata::query()->where(['wisata_id' => $row->id, 'is_main'=>1])->first();
                    @endphp
                        <img src="{{ asset('images/'.$image->foto)}}" style="width:100%; height:250px" alt="">
                        <a href="{{url('detail', $row->id)}}" class="btn theme_btn button_hover">Detail</a>
                    </div>

                        <h4 class="sec_h4">{{$row->nama}}</h4>

                    <h5>{{"Rp. ".number_format($row->harga)}}<small>/orang</small></h5>
                </div>
            </div>
            @endforeach



        </div>
    </div>
</section>
<!--================ Accomodation Area  =================-->
@endsection
