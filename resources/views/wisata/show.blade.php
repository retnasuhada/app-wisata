@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Wisata</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Wisata</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">



                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    <i class="fas fa-archway"></i> Nama Wisata

                                </h4>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <div class="col-sm-3 invoice-col">

                                <address>
                                    <strong>Nama Wisata</strong><br>
                                    Judul<br>
                                    Harga<br>
                                    status
                                </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-9 invoice-col">

                                <address>
                                    <strong>{{$data['wisata']->nama}}</strong><br>
                                    {{$data['wisata']->judul}}<br>
                                    {{"Rp. ".number_format($data['wisata']->harga)}}<br>
                                    @if($data['wisata']->status==1)
                                    Aktif
                                    @else
                                    Tidak Aktif
                                    @endif
                                </address>
                            </div>
                            <!-- /.col -->

                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <!-- Table row -->
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title">Galery</h3>
                                        <a href="{{ url('wisata/galery/add',$data['wisata']->id) }}" class="btn  btn-primary btn-sm float-right">Tambah</a>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table id="example2" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="5%">No</th>
                                                    <th>Gambar</th>
                                                    <th>Foto Utama</th>
                                                    <th width="10%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $no=0;
                                                @endphp
                                                @foreach($data['galery'] as $rows)
                                                @php
                                                $no++;
                                                @endphp
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>
                                                        <img style="width: 150px; height:150px;" src="{{asset('images/'.$rows['foto'])}}">
                                                    </td>
                                                     <td>
                                                    @if($rows->is_main=='1')
                                                    Foto Utama
                                                    @else
                                                    <a href="{{url('wisata/galeri/setMain/'.$rows->id,$data['wisata']->id)}}" class="btn  btn-warning btn-sm">Jaidkan Foto Utama</a>
                                                    @endif
                                                    </td>
                                                    <td>

                                                        <a href="{{ url('wisata/galery/delete',$rows['id']) }}">
                                                            <i class="nav-icon fas fa-trash-alt"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                @endforeach

                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>

                            <div class="col-md-6">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title">Fasilitas</h3>
                                        <a href="{{ url('wisata/fasilitas/add',$data['wisata']->id) }}" class="btn  btn-primary btn-sm float-right">Tambah</a>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Icon</th>
                                                    <th>Fasilitas</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $no=0;
                                                @endphp
                                                @foreach($data['fasilitas'] as $row)
                                                @php
                                                $no++;
                                                @endphp
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>
                                                        <img style="width: 50px; height:50px;" src="{{asset('images/'.$row['fasilitas']['icon'])}}">
                                                    </td>
                                                    <td>{{$row['fasilitas']['nama']}}</td>
                                                    <td>
                                                        <a href="{{ url('wisata/fasilitas/delete/fasilitas',$row['id']) }}">
                                                            <i class="nav-icon fas fa-trash-alt"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                @endforeach

                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>

                        </div>

                        <!-- /.row -->


                    </div>
                    <!-- /.invoice -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>

@endsection
