<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableWisata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wisata', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
            $table->string('judul')->nullable();
            $table->binary('deskripsi')->nullable();
            $table->integer('harga')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });

        Schema::create('galeri_wisata', function (Blueprint $table) {
            $table->id();
            $table->integer('wisata_id')->nullable();
            $table->string('foto')->nullable();
            $table->binary('deskripsi')->nullable();
            $table->boolean('is_main')->default(false);
            $table->timestamps();
        });

        Schema::create('wisata_fasilitas', function (Blueprint $table) {
            $table->id();
            $table->integer('wisata_id')->nullable();
            $table->integer('fasilitas_id')->nullable();
            $table->timestamps();
        });

        Schema::create('fasilitas', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
            $table->string('icon')->nullable();
            $table->binary('deskripsi')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });

        Schema::create('galeri_fasilitas', function (Blueprint $table) {
            $table->id();
            $table->string('foto')->nullable();
            $table->integer('fasilitas_id')->nullable();
            $table->binary('deskripsi')->nullable();
            $table->boolean('is_main')->default(false);
            $table->timestamps();
        });

        Schema::create('jadwal', function (Blueprint $table) {
            $table->id();
            $table->date('tanggal')->nullable();
            $table->integer('wisata_id')->nullable();
            $table->string('start_location')->nullable();
            $table->time('start_time')->nullable();
            $table->integer('min_peserta')->nullable();
            $table->integer('max_peserta')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });

        Schema::create('booking', function (Blueprint $table) {
            $table->id();
            $table->integer('jadwal_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('jumlah')->nullable();
            $table->enum('status',['none','pending','terbayar'])->default('none');
            $table->timestamps();
        });

        Schema::create('tr_booking', function (Blueprint $table) {
            $table->id();
            $table->integer('booking_id')->nullable();
            $table->string('nm_bank_pengirim')->nullable();
            $table->string('nm_pemilik_rekening')->nullable();
            $table->string('bukti')->nullable();
            $table->integer('jumlah')->nullable();
            $table->enum('metode_pembayaran',['none','pending','terbayar'])->default('none');
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_wisata');
    }
}
