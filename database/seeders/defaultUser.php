<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class defaultUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // create default user admin
       \App\Models\User::create([
        'name' => 'admin',
        'role' => 'admin',
        'email' => 'admin@gmail.com',
        'password' => Hash::make('Password123'),
    ]);
    }
}
