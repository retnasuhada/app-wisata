<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });


Route::get('/', [App\Http\Controllers\BaseController::class, 'index'])->name('index');
Route::get('/detail/{id}', [App\Http\Controllers\BaseController::class, 'detail'])->name('umum.detail');
Route::get('/booking/{id}', [App\Http\Controllers\BaseController::class, 'booking'])->name('umum.booking');
Route::post('/booking/store', [App\Http\Controllers\BaseController::class, 'store'])->name('umum.booking.store');
Route::post('/booking/buy', [App\Http\Controllers\BaseController::class, 'buy'])->name('umum.booking.buy');


Route::get('/login', function(){
    return view('login')->name('login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');




Route::group(['prefix' =>'wisata'], function (){
    Route::get('/','WisataController@index')->name('wisata.index');
    Route::get('/add','WisataController@add')->name('wisata.add');
    Route::get('/{id}','WisataController@edit')->name('wisata.edit');
    Route::get('/show/{id}','WisataController@show')->name('wisata.show');
    Route::get('/fasilitas/add/{id}','WisataController@addfasilitas')->name('wisata.fasilitas.add');
    Route::get('/fasilitas/delete/fasilitas/{id}','WisataController@deletefasilitas')->name('wisata.fasilitas.delete');
    Route::post('/','WisataController@store')->name('wisata.store');
    Route::post('/update','WisataController@update')->name('wisata.update');
    Route::post('/delete','WisataController@delete')->name('wisata.delete');
    Route::post('/fasilitas/store','WisataController@storefasilitas')->name('wisata.fasilitas.store');
    Route::get('/galeri/setMain/{id}/{fasilitasId}','WisataController@setMain')->name('wisata.galeri.setMain');
    Route::get('/galery/add/{id}','WisataController@addGalery')->name('wisata.galery.add');
    Route::get('/galery/delete/{id}','WisataController@deleteGalery')->name('wisata.galery.delete');
    Route::post('/galery/store','WisataController@storeGalery')->name('wisata.galery.store');
});

Route::group(['prefix' =>'admin/jadwal'], function (){
    Route::get('/','Admin\JadwalController@index')->name('admin.jadwal');
    Route::get('/add','Admin\JadwalController@add')->name('admin.jadwal.add');
    Route::post('/store','Admin\JadwalController@store')->name('admin.jadwal.store');
    Route::post('/update','Admin\JadwalController@update')->name('admin.jadwal.update');
    Route::get('/edit/{id}','Admin\JadwalController@edit')->name('admin.jadwal.edit');
    Route::get('/show/{id}','Admin\JadwalController@show')->name('admin.jadwal.show');

});

Route::group(['prefix' =>'admin/booking'], function (){
    Route::get('/','Admin\BookingController@index')->name('admin.booking');
    Route::get('/add','Admin\BookingController@add')->name('admin.booking.add');
    Route::post('/store','Admin\BookingController@store')->name('admin.booking.store');
    Route::post('/update','Admin\BookingController@update')->name('admin.booking.update');
    Route::get('/edit/{id}','Admin\BookingController@edit')->name('admin.booking.edit');
    Route::get('/show/{id}','Admin\BookingController@show')->name('admin.booking.show');

});


Route::group(['prefix' =>'profil'], function (){
    Route::get('/','ProfilController@index')->name('profil');
    Route::get('/password','ProfilController@password')->name('profil.password');
    Route::get('/edit','ProfilController@edit')->name('profil.edit');
    Route::post('/update/profil','ProfilController@update')->name('profil.update');
    Route::post('/password/update','ProfilController@passwordUpdate')->name('profil.update-password');
});




Route::group(['prefix' =>'user'], function (){
    Route::get('/','UserController@index')->name('user.index');
    Route::get('/{id}','UserController@edit')->name('user.edit');
    Route::post('/','UserController@store')->name('user.store');
    Route::post('/update','UserController@update')->name('user.update');
});

Route::group(['prefix' =>'tamu'], function (){
    Route::get('/kamar/show/{id}','BaseController@show')->name('tamu.kamar.show');
    Route::post('/kamar/booking','BaseController@booking')->name('tamu.kamar.booking');
    Route::get('/invoice/{id}','BaseController@invoice')->name('tamu.invoice');
    Route::get('/rating/{id}','BaseController@rating')->name('tamu.rating');
    Route::get('/pesanan','BaseController@pesanan')->name('tamu.pesanan');
    Route::post('/rating','BaseController@storeRating')->name('rating.store');
});

Route::group(['prefix' =>'reservasi'], function (){
    Route::get('/','ReservasiController@index')->name('reservasi.index');
    Route::get('/{id}','ReservasiController@show')->name('reservasi.show');
    Route::get('/download/{id}','ReservasiController@download')->name('reservasi.download');
    Route::get('/checkin/{id}','ReservasiController@checkin')->name('reservasi.checkin');
    Route::get('/checkout/{id}','ReservasiController@checkout')->name('reservasi.checkout');
    Route::get('/cancel/{id}','ReservasiController@cancel')->name('reservasi.cancel');
    Route::post('/','ReservasiController@store')->name('reservasi.store');
    Route::post('/update','ReservasiController@update')->name('reservasi.update');
});


Route::group(['prefix' =>'fasilitas'], function (){
    Route::get('/','FasilitasController@index')->name('fasilitas.index');
    Route::get('/add','FasilitasController@add')->name('fasilitas.add');
    Route::get('/{id}','FasilitasController@show')->name('fasilitas.show');
    Route::get('/edit/{id}','FasilitasController@edit')->name('fasilitas.edit');
    Route::get('/galeri/add/{id}','FasilitasController@addGaleri')->name('fasilitas.galeri.add');
    Route::get('/galeri/delete/{id}','FasilitasController@deleteGaleri')->name('fasilitas.galeri.delete');
    Route::get('/galeri/setMain/{id}/{fasilitasId}','FasilitasController@setMain')->name('fasilitas.galeri.setMain');
    Route::post('/galery/store','FasilitasController@storeGalery')->name('fasilitas.galery.store');
    Route::post('/','FasilitasController@store')->name('fasilitas.store');
    Route::post('/update','FasilitasController@update')->name('fasilitas.update');
    Route::post('/delete','FasilitasController@delete')->name('fasilitas.delete');
});



