<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kamar;
use App\Models\TipeKamar;
use App\Models\KamarGaleri;
use App\Models\Tamu;
use App\Models\DetailFasilitasKamar;
use App\Models\Fasilitas;
use App\Models\FasilitasWisata;
use App\Models\GaleriWisata;
use App\Models\Reservasi;
use App\Models\Wisata;
class WisataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // dd('index wisata');
        $kamar = Wisata::query()->where('status',1)->get();

        return view('wisata.index', ['data' => $kamar]);
    }

    public function add()
    {
        // dd('disini');
        // $tipe = TipeKamar::query()->where('status', '1')->get();
        return view('wisata.add', ['data' => null, 'edit' => 0]);
    }


    public function edit($id)
    {
        $tipe = TipeKamar::query()->where('status', '1')->get();
        $old = Kamar::find($id);
        return view('kamar.edit', ['data' => $tipe, 'edit' => 1, 'old' => $old]);
    }

    public function addfasilitas($id)
    {
        // $kamar =Kamar::query()->where('id',$id)->first();
        $fasilitas = Fasilitas::query()->where('status',1)->get();

        return view('wisata.fasilitas', ['fasilitas' => $fasilitas,'id'=>$id]);
    }


    public function storeGalery(Request $request)
    {

        $validatedData = $request->validate([
            'galery' => 'required',
            'deskripsi' => 'required',
        ], [
            'galery.required' => 'Foto Tidak Boleh Kosong!',
            'deskripsi.required' => 'Deskripsi Tidak Boleh Kosong!',
        ]);

        $imageName ='galery-'.time().'.'.$request->file('galery')->extension();
        $request->file('galery')->move(public_path('images'), $imageName);

        $galery = new GaleriWisata();
        $galery->wisata_id = $request->wisata_id;
        $galery->foto = $imageName;
        $galery->deskripsi =  $request->deskripsi;
        $galery->save();

        return redirect()->route('wisata.show',$request->wisata_id)->with('success', 'Data Berhasil Di Tambah');

    }

    public function addGalery($id)
    {

// dd('dii');
        return view('wisata.galery_add', ['id' => $id, 'edit' => 0]);
    }

    public function deleteGalery($id)
    {
        $kamar = GaleriWisata::find($id);
        $wisata_id = $kamar->wisata_id;
        $galery = GaleriWisata::query()->where('id',$id)->delete();
        return redirect()->route('wisata.show',$wisata_id)->with('success', 'Data Berhasil Di Tambah');
    }


    public function show($id)
    {
        $wisata = Wisata::query()->where('id', $id)->first();
        $galery = GaleriWisata::query()->where('wisata_id', $id)->get();
        $fasilitasWisata = FasilitasWisata::query()->where('wisata_id', $id)->get();
        $data = [
            'wisata'=> $wisata,
            'galery' => $galery,
            'fasilitas' => $fasilitasWisata
        ];
        return view('wisata.show', ['data' => $data]);
    }

    public function deletefasilitas($id)
    {
        $kamar = FasilitasWisata::query()->where('id',$id)->first();
        $wisata_id = $kamar->wisata_id;
        $detailFasilitas = FasilitasWisata::query()->where('id',$id)->delete();
        return redirect()->route('wisata.show',$wisata_id)->with('success', 'Data Berhasil Di Tambah');
    }

    public function storefasilitas(Request $request)
    {

        $cek = FasilitasWisata::query()
                        ->where(['wisata_id' => $request->wisata_id, 'fasilitas_id'=> $request->fasilitas_id])
                        ->count();
        if($cek > 0){
            return redirect()->back()->with('error','Fasilitas Sudah Ada Sebelumnya');
        }
        $fasilitas = new FasilitasWisata;
        $fasilitas->wisata_id = $request->wisata_id;
        $fasilitas->fasilitas_id = $request->fasilitas_id;
        $fasilitas->save();

        return redirect()->route('wisata.show',$request->wisata_id)->with('success', 'Data Berhasil Di Tambah');
    }

    public function setMain($id, $wisata_id)
    {
       $galeri = GaleriWisata::query()->where(['wisata_id' => $wisata_id, 'is_main'=>1])->first();
        if($galeri){
            $galeri->is_main = false;
            $galeri->update();

        }

        $newMain = GaleriWisata::query()->where('id',$id)->first();
        $newMain->is_main = true;
        $newMain->update();
        return redirect()->route('wisata.show', $wisata_id)->with('success', 'Data Berhasil Di Edit');
    }


    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'judul' => 'required',
            'harga' => 'required',
            'deskripsi' => 'required',
        ], [
            'judul.required' => 'Judul Tidak Boleh Kosong!',
            'nama.required' => 'Nama  Harus Berupa Teks!',
            'harga.required' => 'Harga  Tidak Boleh Kosong',
            'deskripsi.required' => 'Deskripsi Tidak Boleh Kosong!'
        ]);
        // dd('disini');
        $kamar = new Wisata;
        $kamar->nama = $request->nama;
        $kamar->judul = $request->judul;
        $kamar->deskripsi = $request->deskripsi;
        $kamar->harga = $request->harga;
        $kamar->status =true;
        $kamar->save();

        return redirect()->route('wisata.index')->with('success', 'Data Berhasil Di Tambah');
    }

    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'tipe_id' => 'required',
            'nama_kamar' => 'required',
            'harga' => 'required',
            'jumlah' => 'required',
            'deskripsi' => 'required',
        ], [
            'tipe_id.required' => 'Tipe Kamar Tidak Boleh Kosong!',
            'nama_kamar.required' => 'Nama Kamar Harus Berupa Teks!',
            'harga.required' => 'Harga Kamar Tidak Boleh Kosong',
            'jumlah.required' => 'Jumlah Kamar Tidak Boleh Kosong!',
            'deskripsi.required' => 'Deskripsi Tidak Boleh Kosong!'
        ]);

        $kamar = Kamar::find($request->id);
        $kamar->nama_kamar = $request->nama_kamar;
        $kamar->tipe_id = $request->tipe_id;
        $kamar->jumlah = $request->jumlah;
        $kamar->harga = $request->harga;
        $kamar->deskripsi = $request->deskripsi;
        $kamar->update();

        return redirect()->route('kamar.index')->with('success', 'Data Berhasil Di Tambah');
    }

    public function delete(Request $request)
    {
        $kamar = Kamar::find($request->id);
        $kamar->status = 0;
        $kamar->update();
        return redirect()->route('kamar.index')->with('success', 'Data Berhasil Di Tambah');
    }
}
