<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kamar;
use App\Models\TipeKamar;
use App\Models\KamarGaleri;
use App\Models\Tamu;
use App\Models\User;
use App\Models\DetailFasilitasKamar;
use App\Models\Fasilitas;
use App\Models\Reservasi;
use Facade\Ignition\Tabs\Tab;
use Illuminate\Support\Facades\Hash;

class ProfilController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $user = Auth::user();

        $tamu = Tamu::query()->where('user_id', $user->id)->first();
        $data = [
            'user' => $user,
            'tamu' => $tamu
        ];
        if($user->role=='Visit'){
            return view('user.profil-tamu', ['data' => $data]);
        }
        return view('user.profil', ['data' => $data]);

    }

    public function password()
    {

        return view('user.password');
    }

    public function edit()
    {
        $user = Auth::user();
        $tamu = Tamu::query()->where('user_id', Auth::user()->id)->first();

        if($user->role=='Visit'){
            return view('user.edit-profil-tamu', ['data' => $tamu]);
        }
        return view('user.edit-profil', ['data' => $tamu]);
    }

    public function passwordUpdate(Request $request)
    {
        $validatedData = $request->validate([
            'new_password' => 'required',
            'c_password' => 'required',
        ]);

        $auth = Auth::user();
        $user = User::query()->where('id', $auth->id)->first();
        $new_password = $request->new_password;
        $c_password = $request->c_password;
        $old_password = $request->old_password;

        if ($new_password != $c_password) {
            return redirect()->back()->withErrors(['password konfirmasi tidak sesuai']);
        }
        $old = Hash::make($old_password);
        $user->password = $old;
        $user->update();

        return redirect()->route('login');
    }

    public function update(Request $request)
    {

        $auth = Auth::user();
        $user = User::query()->where('id', $auth->id)->first();
        $tamu = Tamu::query()->where('user_id', $user->id)->first();
        $tamu->nama = $request->nama;
        $tamu->nik = $request->nik;
        $tamu->jenis_kelamin = $request->jenis_kelamin;
        $tamu->alamat = $request->alamat;
        $tamu->telepon = $request->telepon;
        $tamu->update();
        return redirect()->route('profil');
    }
}
