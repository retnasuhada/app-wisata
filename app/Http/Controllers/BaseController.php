<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kamar;
use App\Models\TipeKamar;
use App\Models\KamarGaleri;
use App\Models\Fasilitas;
use App\Models\DetailFasilitasKamar;
use App\Models\FasilitasWisata;
use App\Models\Jadwal;
use App\Models\Tamu;
use App\Models\Review;
use App\Models\Reservasi;
use App\Models\TrBooking;
use App\Models\Wisata;
use Illuminate\Support\Carbon;


class BaseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tipe = Wisata::query()->where('status',1)->get();
        return view('index', ['data' => $tipe]);
    }

    public function rating($id)
    {
        $reservasi = Reservasi::query()->where('id',$id)->first();


        return view('ratings',['data'=> $reservasi]);
    }

    public function storeRating(Request $request)
    {
        $review = new Review;
        $review->star = $request->star;
        $review->review = $request->review;
        $review->tamu_id = $request->tamu_id;
        $review->kamar_id = $request->kamar_id;
        $review->save();

        return redirect()->route('tamu.pesanan');
        // return view('ratings');
    }

    public function store(Request $request)
    {
        if (!Auth::check()) {
            return view('auth.login');
        }
        $booking = new Booking;
        $booking->jadwal_id = $request->jadwal_id;
        $booking->jumlah = $request->jumlah;
        $booking->user_id = auth()->user()->id;
        $booking->status = 'none';
        $booking->save();

        return redirect()->route('tamu.pesanan');
    }

    public function buy(Request $request)
    {
        if (!Auth::check()) {
            return view('auth.login');
        }

        $booking = Booking::query()->where('id', $request->booking_id)->first();
        if($booking){
            $tr_booing = new TrBooking;
            $tr_booing->booking_id = $booking->id;
            $tr_booing->nm_bank_pengirim = $booking->id;
            $tr_booing->nm_pemilik_rekening = $booking->id;

            $imageName ='bukti-'.time().'.'.$request->file('bukti')->extension();
            $request->file('bukti')->move(public_path('images'), $imageName);

            $tr_booing->bukti = $imageName;
            $tr_booing->metode_pembayaran = $booking->id;
            $tr_booing->status = $booking->id;
            $tr_booing->jumlah = $booking->id;
            $tr_booing->save();

            $booking->status = 'pending';
            $booking->update();
        }
        return redirect()->route('tamu.pesanan');
    }

    public function detail($id)
    {
        // $wisata = Wisata::find($id);
        $now = Carbon::now();
        $fasilitas = FasilitasWisata::query()->with('fasilitas','wisata')->where('wisata_id', $id)->get();
        $jadwal = Jadwal::query()->where('wisata_id', $id)->where('tanggal','>',$now)->get();
        return view('detail', ['data' => $fasilitas,'jadwal'=>$jadwal]);
    }


    public function invoice($id)
    {
        $reservasi = Reservasi::find($id);
        $tamu = Tamu::query()->where('id', $reservasi->tamu_id)->first();
        $kamar = Kamar::query()->where('id', $reservasi->kamar_id)->with(['tipeKamar'])->first();

        $data = [
            'tamu' => $tamu,
            'kamar' => $kamar,
            'reservasi' => $reservasi,
        ];
        return view('invoice', ['data' => $data]);
    }

    public function pesanan()
    {

        $booking = Booking::query()->with('jadwal')->where('user_id', auth()->user()->id)->orderBy('id','DESC')->get();
        return view('pesanan', ['data' => $booking]);
    }

}
