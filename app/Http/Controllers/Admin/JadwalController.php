<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kamar;
use App\Models\TipeKamar;
use App\Models\KamarGaleri;
use App\Models\Tamu;
use App\Models\DetailFasilitasKamar;
use App\Models\Fasilitas;
use App\Models\FasilitasWisata;
use App\Models\GaleriWisata;
use App\Http\Controllers\Controller;
use App\Models\Jadwal;
use App\Models\Reservasi;
use App\Models\Wisata;
class JadwalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // dd('index wisata');
        $kamar = Jadwal::query()->with('wisata')->where('status',1)->orderBy('tanggal','DESC')->get();
        return view('jadwal.index', ['data' => $kamar]);
    }

    public function add()
    {

        $wisata = Wisata::query()->where('status', '1')->get();
        return view('jadwal.add', ['data' => $wisata, 'edit' => 0]);
    }


    public function edit($id)
    {
        $wisata = Wisata::query()->where('status', '1')->get();
       $old = Jadwal::query()->with('wisata')->where('id', $id)->first();
        return view('jadwal.edit', ['data' => $wisata,'edit' => 1, 'old' => $old]);
    }

    public function addfasilitas($id)
    {
        // $kamar =Kamar::query()->where('id',$id)->first();
        $fasilitas = Fasilitas::query()->where('status',1)->get();

        return view('wisata.fasilitas', ['fasilitas' => $fasilitas,'id'=>$id]);
    }


    public function storeGalery(Request $request)
    {

        $validatedData = $request->validate([
            'galery' => 'required',
            'deskripsi' => 'required',
        ], [
            'galery.required' => 'Foto Tidak Boleh Kosong!',
            'deskripsi.required' => 'Deskripsi Tidak Boleh Kosong!',
        ]);

        $imageName ='galery-'.time().'.'.$request->file('galery')->extension();
        $request->file('galery')->move(public_path('images'), $imageName);

        $galery = new GaleriWisata();
        $galery->wisata_id = $request->wisata_id;
        $galery->foto = $imageName;
        $galery->deskripsi =  $request->deskripsi;
        $galery->save();

        return redirect()->route('wisata.show',$request->wisata_id)->with('success', 'Data Berhasil Di Tambah');

    }

    public function addGalery($id)
    {

// dd('dii');
        return view('wisata.galery_add', ['id' => $id, 'edit' => 0]);
    }

    public function deleteGalery($id)
    {
        $kamar = GaleriWisata::find($id);
        $wisata_id = $kamar->wisata_id;
        $galery = GaleriWisata::query()->where('id',$id)->delete();
        return redirect()->route('wisata.show',$wisata_id)->with('success', 'Data Berhasil Di Tambah');
    }


    public function show($id)
    {
        $wisata = Wisata::query()->where('id', $id)->first();
        $galery = GaleriWisata::query()->where('wisata_id', $id)->get();
        $fasilitasWisata = FasilitasWisata::query()->where('wisata_id', $id)->get();
        $data = [
            'wisata'=> $wisata,
            'galery' => $galery,
            'fasilitas' => $fasilitasWisata
        ];
        return view('wisata.show', ['data' => $data]);
    }

    public function deletefasilitas($id)
    {
        $kamar = FasilitasWisata::query()->where('id',$id)->first();
        $wisata_id = $kamar->wisata_id;
        $detailFasilitas = FasilitasWisata::query()->where('id',$id)->delete();
        return redirect()->route('wisata.show',$wisata_id)->with('success', 'Data Berhasil Di Tambah');
    }

    public function storefasilitas(Request $request)
    {

        $cek = FasilitasWisata::query()
                        ->where(['wisata_id' => $request->wisata_id, 'fasilitas_id'=> $request->fasilitas_id])
                        ->count();
        if($cek > 0){
            return redirect()->back()->with('error','Fasilitas Sudah Ada Sebelumnya');
        }
        $fasilitas = new FasilitasWisata;
        $fasilitas->wisata_id = $request->wisata_id;
        $fasilitas->fasilitas_id = $request->fasilitas_id;
        $fasilitas->save();

        return redirect()->route('wisata.show',$request->wisata_id)->with('success', 'Data Berhasil Di Tambah');
    }


    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'wisata_id' => 'required',
            'tanggal' => 'required',
            'start_location' => 'required',
            'start_time' => 'required',
            'min_peserta' => 'required',
            'max_peserta' => 'required',
        ], [
            'wisata_id.required' => 'Wisata Tidak Boleh Kosong!',
            'tanggal.required' => 'Tanggal Keberangkatan Harus diisi',
            'start_location.required' => 'Titik Kumpul harus diisi',
            'min_peserta.required' => 'Minimal Peserta Tidak Boleh Kosong!',
            'max_peserta.required' => 'maximal Peserta  Harus Berupa Teks!',
            'start_time.required' => 'jam Keberangkatan Tidak Boleh Kosong!'
        ]);

        $jadwalWisata = new Jadwal;
        $jadwalWisata->wisata_id = $request->wisata_id;
        $jadwalWisata->tanggal = $request->tanggal;
        $jadwalWisata->start_location = $request->start_location;
        $jadwalWisata->start_time = $request->start_time;
        $jadwalWisata->min_peserta = $request->min_peserta;
        $jadwalWisata->max_peserta = $request->max_peserta;
        $jadwalWisata->save();

        return redirect()->route('admin.jadwal')->with('success', 'Data Berhasil Di Tambah');
    }

    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'wisata_id' => 'required',
            'tanggal' => 'required',
            'start_location' => 'required',
            'start_time' => 'required',
            'min_peserta' => 'required',
            'max_peserta' => 'required',
        ], [
            'wisata_id.required' => 'Wisata Tidak Boleh Kosong!',
            'tanggal.required' => 'Tanggal Keberangkatan Harus diisi',
            'start_location.required' => 'Titik Kumpul harus diisi',
            'min_peserta.required' => 'Minimal Peserta Tidak Boleh Kosong!',
            'max_peserta.required' => 'maximal Peserta  Harus Berupa Teks!',
            'start_time.required' => 'jam Keberangkatan Tidak Boleh Kosong!'
        ]);

        $jadwalWisata =Jadwal::query()->where('id', $request->id)->first();
        $jadwalWisata->wisata_id = $request->wisata_id;
        $jadwalWisata->tanggal = $request->tanggal;
        $jadwalWisata->start_location = $request->start_location;
        $jadwalWisata->start_time = $request->start_time;
        $jadwalWisata->min_peserta = $request->min_peserta;
        $jadwalWisata->max_peserta = $request->max_peserta;
        $jadwalWisata->update();

        return redirect()->route('admin.jadwal')->with('success', 'Data Berhasil Di Simpan');
    }

    public function delete(Request $request)
    {
        $kamar = Kamar::find($request->id);
        $kamar->status = 0;
        $kamar->update();
        return redirect()->route('kamar.index')->with('success', 'Data Berhasil Di Tambah');
    }
}
