<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kamar;
use App\Models\TipeKamar;
use App\Models\KamarGaleri;
use App\Models\Fasilitas;
use App\Models\GaleriFasilitas;

class FasilitasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $fasilitas = Fasilitas::query()->where('status', 1)->get();

        return view('fasilitas.index', ['data' => $fasilitas]);
    }

    public function add()
    {
        return view('fasilitas.add');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'icon' => 'required',
            'deskripsi' => 'required',
        ], [
            'nama.required' => 'Nama Fasilitas Tidak Boleh Kosong!',
            'deskripsi.required' => 'Deskripsi Tidak Boleh Kosong!',
            'icon.required' => 'Icon Tidak Boleh Kosong!',
        ]);


        $iconName = 'icon-' . time() . '.' . $request->file('icon')->extension();
        $request->file('icon')->move(public_path('images'), $iconName);

        $fasilitas = new Fasilitas;
        $fasilitas->nama = $request->nama;
        $fasilitas->icon = $iconName;
        $fasilitas->deskripsi = $request->deskripsi;
        $fasilitas->status = true;
        $fasilitas->save();



        return redirect()->route('fasilitas.index')->with('success', 'Data Berhasil Di Tambah');
    }

    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required'
        ], [
            'nama.required' => 'Nama Fasilitas Tidak Boleh Kosong!',
            'deskripsi.required' => 'Deskripsi Failitas Tidak Boleh Kosong!',
        ]);



        $fasilitas = Fasilitas::find($request->id);
        $fasilitas->nama = $request->nama;
        $fasilitas->deskripsi = $request->deskripsi;


        if ($request->file('icon') != null || $request->file('icon') != "") {
            $iconName = 'icon-' . time() . '.' . $request->file('icon')->extension();
            $request->file('icon')->move(public_path('images'), $iconName);
            $fasilitas->icon = $iconName;
        }

        $fasilitas->update();

        return redirect()->route('fasilitas.index')->with('success', 'Data Berhasil Di Edit');
    }

    public function storeGalery(Request $request)
    {

        $validatedData = $request->validate([
            'galery' => 'required',
            'deskripsi' => 'required',
        ], [
            'galery.required' => 'Foto / Galery Tidak Boleh Kosong!',
            'deskripsi.required' => 'Deskripsi Tidak Boleh Kosong!',
        ]);
        $imageName = 'galery-' . time() . '.' . $request->file('galery')->extension();
        $request->file('galery')->move(public_path('images'), $imageName);

        $galery = new GaleriFasilitas;
        $galery->fasilitas_id = $request->fasilitas_id;
        $galery->foto = $imageName;
        $galery->deskripsi = $request->deskripsi;
        $galery->save();

        return redirect()->route('fasilitas.show', $request->fasilitas_id)->with('success', 'Data Berhasil Di Tambah');
    }

    public function delete(Request $request)
    {

        $fasilitas = Fasilitas::find($request->id);
        $fasilitas->status = 0;
        $fasilitas->update();
        return redirect()->route('fasilitas.index')->with('success', 'Data Berhasil Di Edit');
    }

    public function setMain($id, $fasilitas_id)
    {
       $galeri = GaleriFasilitas::query()->where(['fasilitas_id' => $fasilitas_id, 'is_main'=>1])->first();
        if($galeri){
            $galeri->is_main = false;
            $galeri->update();

        }

        $newMain = GaleriFasilitas::query()->where('id',$id)->first();
        $newMain->is_main = true;
        $newMain->update();
        return redirect()->route('fasilitas.show', $fasilitas_id)->with('success', 'Data Berhasil Di Edit');
    }

    public function deleteGaleri($id)
    {

        $newMain = GaleriFasilitas::query()->where('id',$id)->first();
        $fasilitas_id = $newMain->fasilitas_id;
        $delete = GaleriFasilitas::query()->where('id', $id)->delete();
        return redirect()->route('fasilitas.show', $fasilitas_id)->with('success', 'Data Berhasil Di Edit');
    }

    public function show($id)
    {
        $fasilitas = Fasilitas::query()->where('id', $id)->first();
        $galeri = GaleriFasilitas::query()->where('fasilitas_id', $id)->get();
        return view('fasilitas.show', ['edit' => 1, 'fasilitas' => $fasilitas, 'galeri' => $galeri]);
    }

    public function addGaleri($id)
    {

        return view('fasilitas.galery_add', ['edit' => 1, 'id' => $id]);
    }

    public function edit($id)
    {
        // $tipe = TipeKamar::query()->where('status', '1')->get();
        $old = Fasilitas::find($id);
        return view('fasilitas.edit', ['edit' => 1, 'old' => $old]);
    }
}
