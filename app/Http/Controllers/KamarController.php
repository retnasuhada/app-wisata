<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kamar;
use App\Models\TipeKamar;
use App\Models\KamarGaleri;
use App\Models\Tamu;
use App\Models\DetailFasilitasKamar;
use App\Models\Fasilitas;
use App\Models\Reservasi;

class KamarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $kamar = Kamar::query()->where('status',1)->whereHas('tipeKamar')->with('tipeKamar')->get();

        return view('kamar.index', ['data' => $kamar]);
    }

    public function add()
    {
        // dd('disini');
        $tipe = TipeKamar::query()->where('status', '1')->get();
        return view('kamar.add', ['data' => $tipe, 'edit' => 0]);
    }


    public function edit($id)
    {
        $tipe = TipeKamar::query()->where('status', '1')->get();
        $old = Kamar::find($id);
        return view('kamar.edit', ['data' => $tipe, 'edit' => 1, 'old' => $old]);
    }

    public function addfasilitas($id)
    {
        $kamar =Kamar::query()->where('id',$id)->first();
        $fasilitas = Fasilitas::query()->where('jenis_fasilitas','Kamar')->get();
        $data = [
            'fasilitas'=>$fasilitas,
            'kamar'=>$kamar
        ];
        return view('kamar.fasilitas', ['data' => $data]);
    }


    public function storeGalery(Request $request)
    {
        $validatedData = $request->validate([
            'galery' => 'required',
        ], [
            'galery.required' => 'Tipe Kamar Tidak Boleh Kosong!',
        ]);

        $imageName ='galery-'.time().'.'.$request->file('galery')->extension();
        $request->file('galery')->move(public_path('images'), $imageName);

        $galery = new KamarGaleri;
        $galery->kamar_id = $request->kamar_id;
        $galery->foto = $imageName;
        $galery->is_active = true;
        $galery->save();

        return redirect()->route('kamar.show',$request->kamar_id)->with('success', 'Data Berhasil Di Tambah');

    }

    public function addGalery($id)
    {
        $kamar = Kamar::find($id);
        $data = [
            'kamar' => $kamar
        ];

        return view('kamar.galery_add', ['data' => $data]);
    }

    public function deleteGalery($id)
    {
        $kamar = KamarGaleri::find($id);
        $kamar_id = $kamar->kamar_id;
        $galery = KamarGaleri::query()->where('id',$id)->delete();
        return redirect()->route('kamar.show',$kamar_id)->with('success', 'Data Berhasil Di Tambah');
    }


    public function show($id)
    {
        $kamar = Kamar::query()->where('id',$id)->with('tipeKamar')->first();
        $detailFasilitas = DetailFasilitasKamar::query()
            ->where('kamar_id', $id)
            ->with('fasilitas')->get();
        $jumlah = $kamar->jumlah;
        $reserv = 0;
        $available = $kamar->jumlah;
        $reservasi = Reservasi::query()->where(['kamar_id'=> $kamar->id,'status'=>'CHECKIN'])->get();
        foreach($reservasi as $booking){
            $jumlah += $booking->qty_kamar;
            $reserv += $booking->qty_kamar;

        }
        $galery = KamarGaleri::query()->where('kamar_id', $id)->get();
        $data = [
            'fasilitasKamar' => $detailFasilitas,
            'id'=>$id,
            'galery'=>$galery,
            'kamar'=>$kamar,
            'available' => $available,
            'reservasi' => $reserv,
            'total' => $jumlah
        ];
        return view('kamar.show', ['data' => $data]);
    }

    public function deletefasilitas($id)
    {
        $kamar = DetailFasilitasKamar::query()->where('id',$id)->first();
        $kamar_id = $kamar->id;
        $detailFasilitas = DetailFasilitasKamar::query()->where('id',$id)->delete();
        return redirect()->route('kamar.show',$kamar_id)->with('success', 'Data Berhasil Di Tambah');
    }



    public function storefasilitas(Request $request)
    {
        $cek = DetailFasilitasKamar::query()
                        ->where(['kamar_id' => $request->kamar_id, 'fasilitas_id'=> $request->fasilitas_id])
                        ->count();
        if($cek > 0){
            return redirect()->back()->with('error','Fasilitas Sudah Ada Sebelumnya');
        }
        $fasilitas = new DetailFasilitasKamar;
        $fasilitas->kamar_id = $request->kamar_id;
        $fasilitas->fasilitas_id = $request->fasilitas_id;
        $fasilitas->save();

        return redirect()->route('kamar.show',$request->kamar_id)->with('success', 'Data Berhasil Di Tambah');
    }


    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'tipe_id' => 'required',
            'nama_kamar' => 'required',
            'harga' => 'required',
            'jumlah' => 'required',
            'deskripsi' => 'required',
        ], [
            'tipe_id.required' => 'Tipe Kamar Tidak Boleh Kosong!',
            'nama_kamar.required' => 'Nama Kamar Harus Berupa Teks!',
            'harga.required' => 'Harga Kamar Tidak Boleh Kosong',
            'jumlah.required' => 'Jumlah Kamar Tidak Boleh Kosong!',
            'deskripsi.required' => 'Deskripsi Tidak Boleh Kosong!'
        ]);

        $kamar = new Kamar;
        $kamar->nama_kamar = $request->nama_kamar;
        $kamar->tipe_id = $request->tipe_id;
        $kamar->jumlah = $request->jumlah;
        $kamar->harga = $request->harga;
        $kamar->deskripsi = $request->deskripsi;
        $kamar->save();

        return redirect()->route('kamar.index')->with('success', 'Data Berhasil Di Tambah');
    }

    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'tipe_id' => 'required',
            'nama_kamar' => 'required',
            'harga' => 'required',
            'jumlah' => 'required',
            'deskripsi' => 'required',
        ], [
            'tipe_id.required' => 'Tipe Kamar Tidak Boleh Kosong!',
            'nama_kamar.required' => 'Nama Kamar Harus Berupa Teks!',
            'harga.required' => 'Harga Kamar Tidak Boleh Kosong',
            'jumlah.required' => 'Jumlah Kamar Tidak Boleh Kosong!',
            'deskripsi.required' => 'Deskripsi Tidak Boleh Kosong!'
        ]);

        $kamar = Kamar::find($request->id);
        $kamar->nama_kamar = $request->nama_kamar;
        $kamar->tipe_id = $request->tipe_id;
        $kamar->jumlah = $request->jumlah;
        $kamar->harga = $request->harga;
        $kamar->deskripsi = $request->deskripsi;
        $kamar->update();

        return redirect()->route('kamar.index')->with('success', 'Data Berhasil Di Tambah');
    }

    public function delete(Request $request)
    {
        $kamar = Kamar::find($request->id);
        $kamar->status = 0;
        $kamar->update();
        return redirect()->route('kamar.index')->with('success', 'Data Berhasil Di Tambah');
    }
}
