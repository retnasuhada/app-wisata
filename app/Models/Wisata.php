<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Wisata extends Model
{
    use HasFactory;

    protected $table = 'wisata';
    protected $fillable = [
        'nama',
        'judul',
        'deskripsi',
        'harga',
        'status'
    ];

    public function galery(){
        return $this->belongsTo('App\Models\GaleriWisata', 'wisata_id', 'id')->where('is_main',1);
    }

    public function checkIn($qty){
       $jumlah = $this->jumlah;
       $this->jumlah = $jumlah - $qty;
       $this->update();
    }

    public function checkOut($qty){
        $jumlah = $this->jumlah;
        $this->jumlah = $jumlah + $qty;
        $this->update();
     }
}
