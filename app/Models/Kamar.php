<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Kamar extends Model
{
    use HasFactory;

    protected $table = 'kamars';
    protected $fillable = [
        'nama_kamar',
        'harga',
        'jumlah',
        'deskripsi',
        'tipe_id',
        'status'
    ];

    public function tipeKamar(){
        return $this->belongsTo('App\Models\TipeKamar', 'tipe_id', 'id');
    }

    public function galery(){
        return $this->hasMany('App\Models\KamarGaleri', 'kamar_id', 'id');
    }

    public function foto(){
        return $this->belongsTo('App\Models\KamarGaleri', 'id', 'kamar_id');
    }

    public function checkIn($qty){
       $jumlah = $this->jumlah;
       $this->jumlah = $jumlah - $qty;
       $this->update();
    }

    public function checkOut($qty){
        $jumlah = $this->jumlah;
        $this->jumlah = $jumlah + $qty;
        $this->update();
     }
}
