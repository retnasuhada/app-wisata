<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Reservasi extends Model
{
    use HasFactory;

    protected $table = 'reservasis';
    protected $fillable = [
        'start_date',
        'end_date',
        'lama',
        'qty_kamar',
        'status',
        'approved_by',
        'approved_date',
        'canceled_by',
        'canceled_date',
        'tamu_id',
        'kamar_id'
    ];

    public function kamar(){
        return $this->belongsTo('App\Models\Kamar', 'kamar_id', 'id')->with('tipeKamar');
    }

    public function tamu(){
        return $this->belongsTo('App\Models\Tamu', 'tamu_id', 'id');
    }

    
}
