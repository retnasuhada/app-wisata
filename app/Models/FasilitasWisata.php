<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FasilitasWisata extends Model
{
    use HasFactory;

    protected $table = 'wisata_fasilitas';
    protected $fillable = [
        'wisata_id',
        'fasilitas_id'
    ];

    public function fasilitas(){
        return $this->belongsTo('App\Models\Fasilitas', 'fasilitas_id', 'id');
    }

    public function wisata(){
        return $this->belongsTo('App\Models\Wisata', 'wisata_id', 'id');
    }

}
