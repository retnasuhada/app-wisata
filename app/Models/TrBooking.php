<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TrBooking extends Model
{
    use HasFactory;

    protected $table = 'tr_booking';
    protected $fillable = [
        'booking_id',
        'nm_bank_pengirim',
        'nm_pemilik_rekening',
        'bukti',
        'metode_pembayaran',
        'status',
        'jumlah'
    ];

    public function booking(){
        return $this->belongsTo('App\Models\Booking', 'booking_id', 'id')->with('jadwal');
    }

}
