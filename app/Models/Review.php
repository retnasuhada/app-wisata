<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Review extends Model
{
    use HasFactory;

    protected $table = 'reviews';
    protected $fillable = [
        'review',
        'star',
        'tamu_id',
        'kamar_id'
    ];

    public function tamu(){
        return $this->belongsTo('App\Models\Tamu', 'tamu_id', 'id');
    }

}
