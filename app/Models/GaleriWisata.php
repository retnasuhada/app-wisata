<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GaleriWisata extends Model
{
    use HasFactory;

    protected $table = 'galeri_wisata';
    protected $fillable = [
        'wisata_id',
        'foto',
        'deskripsi',
        'is_main'
    ];


}
