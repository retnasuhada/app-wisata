<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Jadwal extends Model
{
    use HasFactory;

    protected $table = 'jadwal';
    protected $fillable = [
        'tanggal',
        'wisata_id',
        'start_location',
        'start_time',
        'min_peserta',
        'max_peserta',
        'status',
    ];

    public function wisata(){
        return $this->belongsTo('App\Models\Wisata', 'wisata_id', 'id')->with('galery');
    }

}
