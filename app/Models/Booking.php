<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Booking extends Model
{
    use HasFactory;

    protected $table = 'booking';
    protected $fillable = [
        'jadwal_id',
        'user_id',
        'jumlah',
        'status'
    ];

    public function jadwal(){
        return $this->belongsTo('App\Models\Jadwal', 'jadwal_id', 'id')->with('wisata');
    }

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

}
