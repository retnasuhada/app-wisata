<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GaleriFasilitas extends Model
{
    use HasFactory;

    protected $table = 'galeri_fasilitas';
    protected $fillable = [
        'fasilitas_id',
        'foto',
        'deskripsi',
        'is_main'
    ];


}
