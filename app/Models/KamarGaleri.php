<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class KamarGaleri extends Model
{
    use HasFactory;

    protected $table = 'kamar_galerys';
    protected $fillable = [
        'url',
        'foto',
        'is_active',
        'kamar_id'
    ];


}
